package jruchel.atmsym.listeners;

public interface MessageMedium {
    void displayMessage(String message);
}
