package jruchel.atmsym.utils;

public interface LoadingScreenDisplayer {

    void show();

    void hide();
}
