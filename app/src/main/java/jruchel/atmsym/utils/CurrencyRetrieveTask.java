package jruchel.atmsym.utils;

import android.os.AsyncTask;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import jruchel.atmsym.repositories.ResourceManager;


public class CurrencyRetrieveTask extends AsyncTask<String, Void, String> {

    private Runnable onLoadListener;

    @Override
    protected String doInBackground(String... strings) {
        try {
            ResourceManager.getInstance().setExchangeRates(getExchangeRates());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onPostExecute(String s) {
        onLoadListener.run();
    }

    public void setOnLoadListener(Runnable onLoadListener) {
        this.onLoadListener = onLoadListener;
    }

    private Map<String, Double> getExchangeRates() throws IOException {
        //API Url to download exchange rates with USD as base
        String url_str = "https://api.exchangerate-api.com/v4/latest/USD";

        URL url = new URL(url_str);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.getContent();

        JsonElement root = JsonParser.parseReader(new InputStreamReader((InputStream) conn.getContent()));
        JsonObject jsonObject = root.getAsJsonObject();

        String content = jsonObject.toString();

        return getRatesAsMap(content);
    }

    private String getRates(String json) {
        String[] rates = json.split("rates");
        String result = rates[1].substring(2, rates[1].length() - 1);
        return result;
    }

    private Map<String, Double> getRatesAsMap(String json) {
        json = getRates(json);
        json = json.substring(1, json.length() - 1);
        String[] entries = json.split(",");
        Map<String, Double> rates = new HashMap<>();
        for (String e : entries) {
            String[] entry = e.split(":");
            String key = entry[0];
            key = key.substring(1, key.length() - 1);
            Double val = 1 / Double.valueOf(entry[1]);
            rates.put(key, val);
        }
        return rates;
    }
}
