package jruchel.atmsym.utils;

import android.content.Context;

public class StringsResSupplier {

    private Context context;
    private static StringsResSupplier instance;

    public static void initialize(Context context) {
        instance = new StringsResSupplier();
        instance.context = context;
    }

    public static StringsResSupplier getInstance() {
        return instance;
    }

    public String getString(int resID) {
        return context.getString(resID);
    }

}
