package jruchel.atmsym.tasks;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import jruchel.atmsym.utils.LoadingScreenDisplayer;

public class LoadingScreen {

    private List<AsyncTask> tasks;
    private LoadingScreenDisplayer displayer;
    private Runnable calculations;

    public LoadingScreen(LoadingScreenDisplayer displayer) {
        this.displayer = displayer;
        tasks = new ArrayList<>();
    }

    public void setCalculations(Runnable calculations) {
        this.calculations = calculations;
    }

    public void addTask(AsyncTask task) {
        tasks.add(task);
    }

    public void setDisplayer(LoadingScreenDisplayer displayer) {
        this.displayer = displayer;
    }


    public void start() {
        displayer.show();
        boolean isDone = false;

        for(AsyncTask t: tasks) {
            t.execute();
        }

        while (!isDone) {
            isDone = true;
            for (AsyncTask t : tasks) {
                if (!t.getStatus().equals(AsyncTask.Status.FINISHED)) {
                    isDone = false;
                }
            }
        }
        displayer.hide();

    }
}
