package jruchel.atmsym.tasks;

import android.os.AsyncTask;

import jruchel.atmsym.database.LogInResponse;
import jruchel.atmsym.database.ResponseConsumer;

public class LogInTask extends AsyncTask {

    private boolean isFinished = false;
    private Runnable request;
    private ResponseConsumer responseConsumer;


    public LogInTask(Runnable request, ResponseConsumer responseConsumer) {
        this.request = request;
        setConsumer(responseConsumer);
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        request.run();

        while (!isFinished) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
                isFinished = true;
            }
        }

        return null;
    }

    public void setConsumer(final ResponseConsumer consumer) {
        this.responseConsumer = new ResponseConsumer() {
            @Override
            public void consumeResponse(LogInResponse response) {
                consumer.consumeResponse(response);
                isFinished = true;
            }
        };
    }

    public ResponseConsumer getResponseConsumer() {
        return responseConsumer;
    }

    public Runnable getRequest() {
        return request;
    }

    public void setRequest(Runnable request) {
        this.request = request;
    }
}
