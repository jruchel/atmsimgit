package jruchel.atmsym.tasks;

import android.os.AsyncTask;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jruchel.atmsym.repositories.ResourceManager;
import jruchel.atmsym.utils.LoadingScreenDisplayer;

public class BackgroundTasks extends AsyncTask implements LoadingScreenDisplayer {

    private LoadingScreenDisplayer display;
    private boolean isFinished = false;
    private List<Runnable> tasks;
    private List<Runnable> postTasks;

    public BackgroundTasks(LoadingScreenDisplayer display) {
        this.setDisplay(display);
    }

    public BackgroundTasks() {
        postTasks = new ArrayList<>();
        tasks = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        if (display != null) {
            this.show();
        }
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        try {
            ResourceManager.getInstance().setExchangeRates(getExchangeRates());
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Runnable r : tasks) {
            r.run();
        }

        while (!isFinished) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        for (Runnable r : postTasks) {
            r.run();
        }
        if (display != null) {
            this.hide();
        }
    }

    private Map<String, Double> getExchangeRates() throws IOException {
        //API Url to download exchange rates with USD as base
        String url_str = "https://api.exchangerate-api.com/v4/latest/USD";

        URL url = new URL(url_str);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.getContent();

        JsonElement root = JsonParser.parseReader(new InputStreamReader((InputStream) conn.getContent()));
        JsonObject jsonObject = root.getAsJsonObject();

        String content = jsonObject.toString();

        return getRatesAsMap(content);
    }

    private String getRates(String json) {
        String[] rates = json.split("rates");
        String result = rates[1].substring(2, rates[1].length() - 1);
        return result;
    }

    private Map<String, Double> getRatesAsMap(String json) {
        json = getRates(json);
        json = json.substring(1, json.length() - 1);
        String[] entries = json.split(",");
        Map<String, Double> rates = new HashMap<>();
        for (String e : entries) {
            String[] entry = e.split(":");
            String key = entry[0];
            key = key.substring(1, key.length() - 1);
            Double val = 1 / Double.valueOf(entry[1]);
            rates.put(key, val);
        }
        return rates;
    }

    public void addTask(Runnable runnable) {
        tasks.add(runnable);
    }

    public void addTasks(Collection<Runnable> tasks) {
        this.tasks.addAll(tasks);
    }

    public void addPostTask(Runnable runnable) {
        postTasks.add(runnable);
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setDisplay(LoadingScreenDisplayer display) {
        this.display = display;
    }

    public void setFinished() {
        isFinished = true;
    }

    @Override
    public void show() {
        display.show();
    }

    @Override
    public void hide() {
        display.hide();
    }
}
