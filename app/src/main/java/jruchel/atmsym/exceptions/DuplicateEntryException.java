package jruchel.atmsym.exceptions;


import jruchel.atmsym.R;
import jruchel.atmsym.utils.StringsResSupplier;

public class DuplicateEntryException extends Exception {

    public DuplicateEntryException() {
        super(StringsResSupplier.getInstance().getString(R.string.DuplicateEntryMessage));
    }

    public DuplicateEntryException(String msg) {
        super(msg);
    }

}
