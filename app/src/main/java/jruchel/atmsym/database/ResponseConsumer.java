package jruchel.atmsym.database;


public interface ResponseConsumer {
    void consumeResponse(LogInResponse response);
}
