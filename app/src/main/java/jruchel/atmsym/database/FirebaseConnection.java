package jruchel.atmsym.database;

import com.example.databaseconnectionlibrary.DatabaseConnection;
import com.google.firebase.firestore.FirebaseFirestore;

public class FirebaseConnection implements DatabaseConnection<FirebaseFirestore> {

    private static FirebaseConnection instance;

    public static FirebaseConnection getInstance() {
        return instance == null ? instance = new FirebaseConnection() : instance;
    }

    @Override
    public FirebaseFirestore getConnection() {
        return FirebaseFirestore.getInstance();
    }
}
