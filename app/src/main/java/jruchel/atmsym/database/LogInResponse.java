package jruchel.atmsym.database;

public enum LogInResponse {
    SUCCESS(1), WRONG_PIN(-1), ACC_NOT_FOUND(0);

    int code;

    LogInResponse(int i) {
        this.code = i;
    }

    public int getCode() {
        return code;
    }
}
