package jruchel.atmsym.database;

import androidx.annotation.NonNull;

import com.example.databaseconnectionlibrary.Database;
import com.example.databaseconnectionlibrary.DatabaseOperator;
import com.example.databaseconnectionlibrary.Entry;
import com.example.databaseconnectionlibrary.ErrorHandler;
import com.example.databaseconnectionlibrary.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import jruchel.atmsym.R;
import jruchel.atmsym.exceptions.DuplicateEntryException;
import jruchel.atmsym.models.Account;
import jruchel.atmsym.utils.StringsResSupplier;

public class FirestoreOperator implements DatabaseOperator {

    private static FirestoreOperator instance;
    private ErrorHandler errorHandler;
    private FirebaseFirestore conn = FirebaseConnection.getInstance().getConnection();

    public static FirestoreOperator getInstance() {
        return instance == null ? instance = new FirestoreOperator() : instance;
    }

    @Override
    public void addEntry(final Entry entry) {
        //Checking if the entry doesn't already exist
        conn.collection("CreditCards")
                .document(entry.getId())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.get("content") == null) {
                            conn.collection("CreditCards")
                                    .document(entry.getId())
                                    .set(entry);
                            errorHandler.throwException(new Exception(StringsResSupplier.getInstance().getString(R.string.accountCreated)));
                        } else {
                            errorHandler.throwException(new DuplicateEntryException());
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        errorHandler.throwException(new Exception(e.getMessage()));
                    }
                });
    }

    @Override
    public void addUser(User user) {

    }

    @Override
    public void removeEntry(Entry entry) {
        conn.collection("CreditCards")
                .document(entry.getId())
                .delete();
    }

    @Override
    public void removeUser(User user) {

    }

    @Override
    public void editEntry(final Entry entry) {
        conn.collection("CreditCards")
                .document(entry.getId())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        conn.collection("CreditCards")
                                    .document(entry.getId())
                                    .set(entry);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        errorHandler.throwException(new Exception(e.getMessage()));
                    }
                });
    }

    @Override
    public void editUser(User user) {

    }

    @Override
    public User getUser(String id) {
        return new User("elo", "320");
    }

    @Override
    public Entry getEntry(String id) {
        final Entry<Account> result = new Entry<>();
        DocumentReference docRef = conn.collection("CreditCards").document(id);

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                try {
                    if (task.isSuccessful()) {

                        DocumentSnapshot documentSnapshot = task.getResult();
                        assert documentSnapshot != null;
                        Map queryResult = documentSnapshot.getData();


                        result.setDate((Date) Objects.requireNonNull(queryResult).get("date"));
                        double balance = (double) Objects.requireNonNull(Objects.requireNonNull(((HashMap) queryResult.get("content")).get("balance")));
                        String ccn = Objects.requireNonNull(Objects.requireNonNull(((HashMap) queryResult.get("content")).get("ccn")).toString());
                        String pin = Objects.requireNonNull(Objects.requireNonNull(((HashMap) Objects.requireNonNull(queryResult.get("content"))).get("pin")).toString());
                        Account account = new Account(ccn, pin);
                        account.setBalance(balance);
                        result.setContent(account);
                        result.setID((String) queryResult.get("id"));
                        Database.getInstance().getDataCallback().callback(result);
                    }
                } catch (NullPointerException ex) {
                    Database.getInstance().getDataCallback().callback(new Entry<>(new Account("null", "null")));
                }

            }
        });
        return result;
    }

    @Override
    public List<Entry> getAllEntries() {
        return null;
    }

    @Override
    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }
}

//TODO remove all occurrences of a specified content field within entry class, content should remain unspecified to allow the user for multiple types of entries
//TODO Make it impossible overwrite entries by adding new ones, if addEntry is used on an existing entry, the operation should be rejected