package jruchel.atmsym.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import jruchel.atmsym.R;
import jruchel.atmsym.models.ATM;

public class AccountActivity extends AppCompatActivity {

    private Button deposit;
    private Button withdraw;
    private Button logout;

    private TextView balanceDisplay;

    private ATM atm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        atm = ATM.getInstance();
        initializeViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        createOptions(menu);
        return true;
    }

    private void createOptions(Menu menu) {
        final List<ATM.Currency> currencies = atm.getCurrencies();
        for (int i = 0; i < currencies.size(); i++) {
            final ATM.Currency temp = currencies.get(i);
            menu.add(Menu.NONE, i, Menu.NONE, temp.getAsISOCode()).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    AccountActivity.this.atm.setSelectedCurrency(temp);
                    Toast.makeText(AccountActivity.this, String.format(getString(R.string.currencySelected), temp.getAsISOCode()), Toast.LENGTH_SHORT).show();
                    AccountActivity.this.updateBalanceDisplay();
                    return true;
                }
            });
        }
    }

    private void initializeViews() {
        balanceDisplay = findViewById(R.id.balanceView);
        updateBalanceDisplay();

        deposit = findViewById(R.id.deposit);
        deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDepositDialog().show();
            }
        });

        withdraw = findViewById(R.id.withdraw);
        withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWithdrawDialog().show();
            }
        });

        logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                atm.logout();
                Intent menu = new Intent(AccountActivity.this, MainActivity.class);
                startActivity(menu);
                finish();
            }
        });
    }

    private AlertDialog showDepositDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(getString(R.string.depositQuestion));

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        dialog.setView(input);
        dialog.setCancelable(false);
        dialog.setPositiveButton(getString(R.string.Accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                double amount;

                try {
                    amount = Double.parseDouble(input.getText().toString());
                } catch (Exception ex) {
                    amount = 0;
                }
                atm.transfer(amount);
                updateBalanceDisplay();
            }
        });

        dialog.setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        return dialog.show();
    }

    private AlertDialog showWithdrawDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(getString(R.string.withdrawlQuestion));

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        dialog.setView(input);
        dialog.setCancelable(false);
        dialog.setPositiveButton(getString(R.string.Accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                double amount;

                try {
                    amount = Double.parseDouble(input.getText().toString());
                } catch (Exception ex) {
                    amount = 0;
                }
                atm.withdraw(amount);
                updateBalanceDisplay();
            }
        });

        dialog.setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        return dialog.show();
    }

    private void updateBalanceDisplay() {
        balanceDisplay.setText(String.format(getString(R.string.Balance), atm.getBalanceAsString()));
    }
}
