package jruchel.atmsym.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.databaseconnectionlibrary.Database;
import com.example.databaseconnectionlibrary.ErrorHandler;

import java.io.IOException;

import jruchel.atmsym.R;
import jruchel.atmsym.database.LogInResponse;
import jruchel.atmsym.database.ResponseConsumer;
import jruchel.atmsym.listeners.MessageMedium;
import jruchel.atmsym.models.ATM;
import jruchel.atmsym.tasks.BackgroundTasks;
import jruchel.atmsym.tasks.LoadingScreen;
import jruchel.atmsym.tasks.LogInTask;
import jruchel.atmsym.utils.LoadingScreenDisplayer;

public class MainActivity extends AppCompatActivity {

    private ATM atm;
    private String ccn, pin;

    //Views
    private EditText pinEditText;
    private EditText ccnEditText;
    private Button logInButton;
    private Button createAccountButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Database.getInstance().setErrorHandler(new ErrorHandler() {
            @Override
            public void throwException(Exception e) {
                Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        try {
            initializeATM();
        } catch (IOException e) {
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        initializeViews();
    }

    private void initializeATM() throws IOException {
        atm = ATM.getInstance();
        atm.setMessageMedium(new MessageMedium() {
            @Override
            public void displayMessage(String message) {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initializeViews() {
        //Text watchers
        TextWatcher ccnTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                ccn = ccnEditText.getText().toString();
            }
        };
        TextWatcher pinTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                pin = pinEditText.getText().toString();
            }
        };

        //EditTexts
        pinEditText = findViewById(R.id.pinEditText);
        pinEditText.addTextChangedListener(pinTextWatcher);

        ccnEditText = findViewById(R.id.ccnEditText);
        ccnEditText.addTextChangedListener(ccnTextWatcher);

        //Buttons
        logInButton = findViewById(R.id.logInButton);
        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createLoginTask(ccn, pin).execute();
                initiateBackgroundTasks().execute();
            }
        });

        createAccountButton = findViewById(R.id.createAccountButton);
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                atm.createAccount(ccn, pin);
            }
        });
    }

    private LoadingScreen createLoadingScreen() {
        final AlertDialog loadingDialog = showLoadingDialog();
        LoadingScreenDisplayer loadingScreenDisplayer = new LoadingScreenDisplayer() {
            @Override
            public void show() {
                loadingDialog.show();
            }

            @Override
            public void hide() {
                loadingDialog.hide();
            }
        };
        return new LoadingScreen(loadingScreenDisplayer);
    }

    private AsyncTask initiateBackgroundTasks() {
        BackgroundTasks loadingScreen = new BackgroundTasks();
        final AlertDialog loadingDialog = showLoadingDialog();
        loadingScreen.setDisplay(new LoadingScreenDisplayer() {
            @Override
            public void show() {
                loadingDialog.show();
            }

            @Override
            public void hide() {
                loadingDialog.hide();
            }
        });
        return loadingScreen;
    }

    private AsyncTask createLoginTask(final String ccn, final String pin) {
        Runnable request = new Runnable() {
            @Override
            public void run() {
                atm.sendLogInRequest(ccn, pin);
            }
        };

        ResponseConsumer responseConsumer = new ResponseConsumer() {
            @Override
            public void consumeResponse(LogInResponse response) {
                switch (response) {
                    case SUCCESS:
                        final Intent accountActivity = new Intent(MainActivity.this, AccountActivity.class);
                        startActivity(accountActivity);
                        finish();
                        break;
                    case WRONG_PIN:
                        Toast.makeText(MainActivity.this, R.string.invalidPIN, Toast.LENGTH_SHORT).show();
                        break;
                    case ACC_NOT_FOUND:
                        Toast.makeText(MainActivity.this, R.string.accountNotFound, Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
        LogInTask logInTask = new LogInTask(request, responseConsumer);
        atm.setResponseConsumer(logInTask.getResponseConsumer());

        return logInTask;
    }

    private AlertDialog showLoadingDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(getString(R.string.loadingMessage));
        return builder.create();
    }
}
