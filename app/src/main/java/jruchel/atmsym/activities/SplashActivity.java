package jruchel.atmsym.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.databaseconnectionlibrary.Database;

import jruchel.atmsym.R;
import jruchel.atmsym.database.FirebaseConnection;
import jruchel.atmsym.database.FirestoreOperator;
import jruchel.atmsym.utils.CurrencyRetrieveTask;
import jruchel.atmsym.utils.StringsResSupplier;

public class SplashActivity extends AppCompatActivity {

    private Runnable onLoadListener;
    private Intent preloadedIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preloadedIntent = new Intent(this, MainActivity.class);
        onLoadListener = new Runnable() {
            @Override
            public void run() {
                startActivity(preloadedIntent);
                finish();
            }
        };
        setUpResources();
    }

    private void setUpResources() {
        Database.setConnection(FirebaseConnection.getInstance());
        Database.getInstance().setOperator(FirestoreOperator.getInstance());
        StringsResSupplier.initialize(this);
        CurrencyRetrieveTask currencyRetrieveTask = new CurrencyRetrieveTask();
        currencyRetrieveTask.setOnLoadListener(onLoadListener);
        currencyRetrieveTask.execute();

    }
}