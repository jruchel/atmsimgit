package jruchel.atmsym.repositories;

import java.util.HashMap;
import java.util.Map;

public class ResourceManager {

    private static ResourceManager instance;

    private static Map<String, Double> exchangeRates = new HashMap<>();

    public static ResourceManager getInstance() {
        return instance == null ? instance = new ResourceManager() : instance;
    }

    public Map<String, Double> getExchangeRates() {
        return exchangeRates;
    }

    public void setExchangeRates(Map<String, Double> exchangeRates) {
        this.exchangeRates = exchangeRates;
    }
}
