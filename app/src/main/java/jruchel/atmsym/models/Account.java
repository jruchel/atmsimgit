package jruchel.atmsym.models;

import java.util.Objects;

public class Account {

    private double balance;
    private String pin;
    //Credit Card Number
    private String ccn;

    public Account(String ccn, String pin) {
        this.ccn = ccn;
        this.pin = pin;
        this.balance = 0;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCcn() {
        return ccn;
    }

    public String getPin() {
        return pin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return pin.equals(account.pin) &&
                ccn.equals(account.ccn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pin, ccn);
    }

    void withdraw(double amount) {
        this.balance -= amount;
    }

    void transfer(double amount) {
        this.balance += amount;
    }
}
