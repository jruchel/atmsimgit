package jruchel.atmsym.models;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.example.databaseconnectionlibrary.DataCallback;
import com.example.databaseconnectionlibrary.Database;
import com.example.databaseconnectionlibrary.Entry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import jruchel.atmsym.R;
import jruchel.atmsym.database.LogInResponse;
import jruchel.atmsym.database.ResponseConsumer;
import jruchel.atmsym.listeners.MessageMedium;
import jruchel.atmsym.repositories.ResourceManager;
import jruchel.atmsym.utils.StringsResSupplier;

public class ATM {

    private static ATM atm;

    //Fields
    private Account request;
    private Account account;

    private Currency selectedCurrency = Currency.USD;

    private MessageMedium messageMedium;

    private StringsResSupplier stringsResSupplier = StringsResSupplier.getInstance();

    //Listeners
    private ResponseConsumer consumer;

    private ATM() {
        DataCallback<Entry<Account>> logInResponse = new DataCallback<Entry<Account>>() {
            @Override
            public void callback(Entry<Account> accountEntry) {
                account = accountEntry.getContent();
                if (account.getCcn().equals("null")) {
                    consumer.consumeResponse(LogInResponse.ACC_NOT_FOUND);
                    return;
                }
                if (verifyLogIn()) {
                    consumer.consumeResponse(LogInResponse.SUCCESS);
                    messageMedium.displayMessage(stringsResSupplier.getString(R.string.LogInSuccessMessage));
                } else {
                    consumer.consumeResponse(LogInResponse.WRONG_PIN);
                    messageMedium.displayMessage(stringsResSupplier.getString(R.string.invalidPIN));
                }
            }
        };
        Database.getInstance().setDataCallback(logInResponse);
        getCurrentRates();
    }

    public static ATM getInstance() {
        return atm == null ? atm = new ATM() : atm;
    }

    public void logout() {
        this.request = null;
        this.account = null;
    }

    public String getCurrency() {
        return selectedCurrency.toString();
    }


    public void setRequest(Account request) {
        this.request = request;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public double getBalance() {
        return account.getBalance() / selectedCurrency.rate;
    }

    /**
     * Verifies whether the request matches the database entry
     *
     * @return true if the user can be logged in, otherwise false
     */
    private boolean verifyLogIn() {
        return request.getCcn().equals(account.getCcn());
    }

    public void withdraw(double amount) {
        account.withdraw(amount * selectedCurrency.rate);
        updateDatabase();
        sendMessage(stringsResSupplier.getString(R.string.WithdrawlSuccessMessage));
    }

    public void transfer(double amount) {
        account.transfer(amount * selectedCurrency.rate);
        updateDatabase();
        sendMessage(stringsResSupplier.getString(R.string.TransferSuccessMessage));
    }

    @SuppressLint("DefaultLocale")
    public String getBalanceAsString() {
        List<Currency> signless = new ArrayList<>();
        signless.add(Currency.PLN);
        signless.add(Currency.SAR);

        if (signless.contains(selectedCurrency)) {
            return String.format("%4.2f %s", getBalance(), selectedCurrency.toString());
        } else {
            return String.format("%s %4.2f", selectedCurrency.toString(), getBalance());
        }

    }

    public void createAccount(String ccn, String pin) {
        if (!verifyDetails(ccn, pin)) {
            return;
        }
        pin = String.valueOf(Objects.hash(pin));
        account = new Account(ccn, pin);
        account.setBalance(1000);
        Entry<Account> entry = new Entry<>();
        entry.setDate(new Date());
        entry.setContent(account);
        entry.setID(String.valueOf(account.hashCode()));
        Database.getInstance().addEntry(entry);
    }

    /**
     * Sends an update of the current state of account to the database
     */
    private void updateDatabase() {
        Entry<Account> update = new Entry<>(account);
        update.setID(String.valueOf(account.hashCode()));
        Database.getInstance().editEntry(update);
    }

    /**
     * Starts the process of log in verification, a ResponseConsumer must be set in order to receive the response
     *
     * @param ccn Credit card number
     * @param pin Credit card PIN
     */
    public void sendLogInRequest(String ccn, String pin) {
        if (!verifyDetails(ccn, pin)) {
            return;
        }
        pin = String.valueOf(Objects.hash(pin));
        request = new Account(ccn, pin);
        Database.getInstance().getEntry(String.valueOf(request.hashCode()));

    }

    private boolean verifyDetails(String ccn, String pin) {
        if (ccn == null) {
            ccn = "";
        }
        if (pin == null) {
            pin = "";
        }
        if (ccn.length() != 4) {
            messageMedium.displayMessage(stringsResSupplier.getString(R.string.invalidCCN));
            return false;
        }
        if (pin.length() != 4) {
            messageMedium.displayMessage(stringsResSupplier.getString(R.string.invalidPIN));
            return false;
        }
        return true;
    }

    /**
     * Allows the user to respond to log in success or failure
     *
     * @param consumer ReponseConsumer interface specifying the response
     */
    public void setResponseConsumer(ResponseConsumer consumer) {
        this.consumer = consumer;
    }

    /**
     * *Optional | Allows the ATM to send various messages like account created and so on
     *
     * @param messageMedium MessageMedium interface specifying how the message should be displayed
     */
    public void setMessageMedium(MessageMedium messageMedium) {
        this.messageMedium = messageMedium;
    }

    private void sendMessage(String message) {
        if (messageMedium != null) {
            messageMedium.displayMessage(message);
        }
    }

    public void setSelectedCurrency(Currency currency) {
        selectedCurrency = currency;
    }

    public List<Currency> getCurrencies() {
        return Arrays.asList(Currency.values());
    }

    private void getCurrentRates() {
        try {
            Map<String, Double> exchangeRates = ResourceManager.getInstance().getExchangeRates();
            for (Currency c : getCurrencies()) {
                c.setRate(exchangeRates.get(c.getAsISOCode()));
            }
        } catch (NullPointerException ignored) {
        }
    }


    public enum Currency {
        PLN(0.261626), USD(1), EUR(1.11425), RUB(0.0160164), SAR(0.261626);

        private double rate;

        Currency(double n) {
            rate = n;
        }

        private void setRate(double rate) {
            this.rate = rate;
        }

        @NonNull
        @Override
        public String toString() {
            switch (this) {
                case PLN:
                    return "zł";
                case USD:
                    return "$";
                case EUR:
                    return "€";
                case RUB:
                    return "\u20BD";
                case SAR:
                    return "SR";
                default:
                    return "@";
            }
        }

        @NonNull
        public String getAsISOCode() {
            switch (this) {
                case PLN:
                    return "PLN";
                case USD:
                    return "USD";
                case EUR:
                    return "EUR";
                case RUB:
                    return "RUB";
                case SAR:
                    return "SAR";
                default:
                    return "CUR";
            }
        }
    }
}